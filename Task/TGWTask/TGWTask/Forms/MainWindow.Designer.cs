﻿namespace TGWTask
{
  partial class MainWindow
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.openFileButton = new System.Windows.Forms.Button();
            this.configInfoRichTextBox = new System.Windows.Forms.RichTextBox();
            this.resetButton = new System.Windows.Forms.Button();
            this.printAllButton = new System.Windows.Forms.Button();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileButton
            // 
            this.openFileButton.Location = new System.Drawing.Point(12, 12);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(95, 23);
            this.openFileButton.TabIndex = 0;
            this.openFileButton.Text = "Open File";
            this.openFileButton.UseVisualStyleBackColor = true;
            this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
            // 
            // configInfoRichTextBox
            // 
            this.configInfoRichTextBox.Location = new System.Drawing.Point(12, 41);
            this.configInfoRichTextBox.Name = "configInfoRichTextBox";
            this.configInfoRichTextBox.Size = new System.Drawing.Size(521, 143);
            this.configInfoRichTextBox.TabIndex = 1;
            this.configInfoRichTextBox.Text = "";
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(456, 12);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 23);
            this.resetButton.TabIndex = 2;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // printAllButton
            // 
            this.printAllButton.Location = new System.Drawing.Point(375, 12);
            this.printAllButton.Name = "printAllButton";
            this.printAllButton.Size = new System.Drawing.Size(75, 23);
            this.printAllButton.TabIndex = 3;
            this.printAllButton.Text = "Print All";
            this.printAllButton.UseVisualStyleBackColor = true;
            this.printAllButton.Click += new System.EventHandler(this.printAllButton_Click);
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(147, 14);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(117, 20);
            this.searchTextBox.TabIndex = 4;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(270, 11);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 5;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 201);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.printAllButton);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.configInfoRichTextBox);
            this.Controls.Add(this.openFileButton);
            this.Name = "MainWindow";
            this.Text = "TASK";
            this.ResumeLayout(false);
            this.PerformLayout();

    }

        #endregion

        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.RichTextBox configInfoRichTextBox;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Button printAllButton;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Button searchButton;
    }
}

