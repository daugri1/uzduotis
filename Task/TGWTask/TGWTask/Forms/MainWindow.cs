﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TGWTask.Classes;

namespace TGWTask
{
    public partial class MainWindow : Form
    {
        private MainWindowControls MainControls = new MainWindowControls();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            MainControls.openFile();
            configInfoRichTextBox.Text = MainControls.printConfig();
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            MainControls.resetValues();
            configInfoRichTextBox.Text = "";
        }

        private void printAllButton_Click(object sender, EventArgs e)
        {
            configInfoRichTextBox.Text = MainControls.printConfig();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            if (searchTextBox.TextLength > 0)
            {
                configInfoRichTextBox.Text = MainControls.searchForConfig(searchTextBox.Text);
            }
        }
    }
}

////- it should provide information if variability constraints are violated 
/// I think there would be a possibility to use an observer design pattern to listen for changes in config file and then check them against predetermined rules for values.