﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TGWTask.Classes
{
    class MainWindowControls
    {
        private ConfigId Config = new ConfigId();
        private List<string> setValues = new List<string>();
        public void openFile()
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "C:\\Users\\Daumantas\\Desktop\\Config";
                openFileDialog.Filter = "txt files (*.txt)|*.txt";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    var fileStream = openFileDialog.OpenFile();
                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        parseValues(reader.ReadToEnd());
                    }
                }
            }
        }

        public void parseValues(string rawConfig)
        {
            string[] lines = rawConfig.Split(new[] { '\r', '\n', '/' }).Where(w => w.Length > 0 && w.Contains(":\t")).ToArray();
            foreach (var line in lines)
            {
                assignValues(line.Replace("\t", ""));
            }
        }


        public void assignValues(string Value)
        {
            Type configIdType = typeof(ConfigId);
            var listOfFieldNames = typeof(ConfigId).GetProperties().Select(f => f.Name).ToList();

            foreach (var Name in listOfFieldNames)
            {
                var Splited = Value.Split(new string[] { Name }, StringSplitOptions.None);

                if (Splited.Count() > 1)
                {
                    PropertyInfo Property = configIdType.GetProperty(Name);
                    try
                    {
                        if (Property.PropertyType.Name == "Int32")
                        {
                            Property.SetValue(Config, int.Parse(Splited.ElementAt(1).TrimStart(':')));
                            if(!setValues.Contains(Name))
                                setValues.Add(Name);
                        }
                        else if (Property.PropertyType.Name == "String")
                        {
                            Property.SetValue(Config, Splited.ElementAt(1).TrimStart(':'));
                            if (!setValues.Contains(Name))
                                setValues.Add(Name);
                        }
                        else if (Property.PropertyType.Name == "TimeSpan")
                        {
                            Property.SetValue(Config, TimeSpan.Parse(Splited.ElementAt(1).TrimStart(':')));
                            if (!setValues.Contains(Name))
                                setValues.Add(Name);
                        }
                        break;
                    }
                    catch
                    {
                        MessageBox.Show(string.Format("{0} Could not be loaded because of wrong DataType, old value was left !", Name));
                        break;
                    }
                }
            }
        }

        public string printConfig()
        {
            string configInfo = "";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(Config))
            {
                string name = descriptor.Name;
                object value = descriptor.GetValue(Config);
                if(setValues.Contains(name))
                    configInfo += string.Format("{0} = {1} \n", name, value);

            }
            return configInfo;
        }

        public string searchForConfig(string searchText)
        {
            if (setValues.Contains(searchText))
                return Config.GetType().GetProperty(searchText).GetValue(Config, null).ToString();
            else
                return "No config with this parameter was loaded.";
        }

        public void resetValues()
        {
            setValues = new List<string>();
            Config = new ConfigId();
        }
    }
}
