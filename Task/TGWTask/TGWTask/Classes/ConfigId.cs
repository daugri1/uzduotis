﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGWTask.Classes
{
    class ConfigId
    {
        public int ordersPerHour { get; set; }
        public int orderLinesPerOrder { get; set; }
        public string inboundStrategy { get; set; }
        public string powerSupply { get; set; }
        public TimeSpan resultStartTime { get; set; }
        public int resultInterval { get; set; }

        public int numberOfAisles { get; set; }
    }
}
